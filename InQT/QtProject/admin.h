#ifndef ADMIN_H
#define ADMIN_H
#include <QDebug>
#include "conexion.h"
#include "query.h"
#include "polygon.h"

class Admin
{
private:
    Conexion con;
    Query data;
    Polygon* toInsert;
public:
    Admin();
    void setPolygonAdmin(Polygon*); //setting a Polygon to administrate in class admin
};

#endif // ADMIN_H

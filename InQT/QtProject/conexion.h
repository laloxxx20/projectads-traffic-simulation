#ifndef CONEXION_H
#define CONEXION_H
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QSqlError>
#include <QDebug>

class Conexion
{
private:
    QSqlDatabase db;
public:
    Conexion();
    void makeConexionDB(); //make conexion with DB, maybe here we can make a SigleTon
};

#endif // CONEXION_H

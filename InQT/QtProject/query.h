#ifndef QUERY_H
#define QUERY_H
#include <stdio.h>
#include <QFile>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QVariant>
#include <iostream>
#include <fstream>
#include <string>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>

using namespace std;

class Query
{
    public:
        Query();
        QString getAllPoints(); // function to get Point of DB(used by KML)
        void setPointsOnePolygon(QString points, QString name); // function to insert a Polygon in DB

};

#endif // QUERY_H

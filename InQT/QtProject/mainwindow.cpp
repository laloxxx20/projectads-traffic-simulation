#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->webView->page()->mainFrame()->addToJavaScriptWindowObject("myObject", this); // here put this object in c++ inside js
    main=new Admin();
}

void MainWindow::on_pushButton_clicked()
{
    ui->webView->reload();
    cout<<"Nothing"<<endl;
}

void MainWindow::on_refreshButton_clicked()
{
    ui->webView->reload();
    ui->webView->settings()->clearMemoryCaches();
}
void MainWindow::on_CreatePolygonPoints_clicked()
{
    this-> latitudePointFig = ui->latitude->text();
    this->longitudePointFig= ui->longitude->text();

}

QString MainWindow::setToPointsPolyToDB(QString textPoints)
{
    QString rpta= textPoints.replace("\n",",");
    return rpta.replace(rpta.size()-1,1,"");
}

/****(S) here We get the part of code HTML when Points was saved *****/
void MainWindow::getPointsOfPolygonHtml()
{
    QWebFrame* frame= ui->webView->page()->mainFrame();
    QString htmlQString = frame->toHtml();    
    QString word="anotherElement"; //just like tokent and identy start in html
    QString tag="</div>Points"; //just like tokent and identy end in html
    QString save="";

    int j = 0;
    int y=0,x=0,yy=0;

    while ((j = htmlQString.indexOf(word, j)) != -1)
    {      
        j++;
        y=j;
    }
    cout<<"j: "<<y<<endl;
    yy=y;

    while ((y = htmlQString.indexOf(tag, y)) != -1)
    {
        y++;
        x=y;
    }
    cout<<"x: "<<x<<endl;

    cout<<"y-j:"<<x-yy<<endl;

    yy+= word.size() +1;
    while(x-2 >= yy)
    {
        save+=htmlQString[yy];
        yy++;
    }

    PointsRightToDbs=setToPointsPolyToDB(save); //setting in variable of Points right to DB;

    /****(S)pass info to object Polygon******/
    OnePolygon=new Polygon();
    OnePolygon->setPoints(PointsRightToDbs);
    main->setPolygonAdmin(OnePolygon);
    /****(E)pass info to object Polygon******/
}
/****(E) here We get the part of code HTML when Points was saved *****/
/**(S)-> function to get point center to kinf of figure in javascript() ******/
void MainWindow::sendPointFigToJs()
{

}

/**(E)-> function to get point center to kinf of figure in javascript() ******/



QString MainWindow::getPointsRightToDbs()
{
    return PointsRightToDbs;
}

MainWindow::~MainWindow()
{
    delete ui;
    delete main;
}


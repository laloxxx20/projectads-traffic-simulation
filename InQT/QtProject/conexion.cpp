#include "conexion.h"

Conexion::Conexion()
{
}

void Conexion::makeConexionDB()
{
    db = QSqlDatabase::addDatabase("QPSQL");
    db.setHostName("localhost");
    db.setDatabaseName("postgres");
    db.setUserName("postgres");
    db.setPassword("123");
    db.setPort(5432);
    bool ok = db.open();
    if (ok)
        qDebug() << "connected to PSQL" << endl;

    else
    {
        qDebug() << "not Connected " << endl;
        qDebug() << db.lastError();
    }
}
